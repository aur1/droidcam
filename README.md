# Droidcam packages for the Arch Linux

<!-- badges: start -->
<!-- badges: end -->

## Usage

### Add repository

Add the following code to `/etc/pacman.conf`:

``` toml
[droidcam]
SigLevel = PackageOptional
Server = https://aur1.gitlab.io/$repo/$arch
```

See also repo listing: <https://aur1.gitlab.io/droidcam/>

### List packages

To show actual packages list:

``` bash
pacman -Sl droidcam
```

### Install packages

To install package:

``` bash
pacman -Syy
pacman -S droidcam-git
```
